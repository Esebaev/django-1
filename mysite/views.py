from django.http.response import HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView

from mysite.models import Dish


def home(request):
    return HttpResponse("<b>Hello World!</b>")

class HomeView(TemplateView):
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = {
            'dishes':Dish.objects.all()
        }
        return context

class Page1View(TemplateView):
    template_name = "page1.html"

    def get_context_data(self, **kwargs):
        context = {
            'names': ['Page1' 'Azamat', 'Pavel', 'Sabina', 'Meerim'],
        }
        return context

class Page2View(TemplateView):
    template_name = "page2.html"

    def get_context_data(self, **kwargs):
        context = {
            'names': ['Page2' 'Azamat', 'Pavel', 'Sabina', 'Meerim'],
        }
        return context

class Page3View(TemplateView):
    template_name = "page3.html"

    def get_context_data(self, **kwargs):
        context = {
            'names': ['Page3' 'Azamat', 'Pavel', 'Sabina', 'Meerim'],
        }
        return context
