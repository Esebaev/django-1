from django.conf.urls import include, url
from django.contrib import admin

from mysite.views import Page1View, Page2View, Page3View, HomeView

urlpatterns = [
    url(r'^$', HomeView.as_view()),
    url(r'page1/', Page1View.as_view()),
    url(r'page2/', Page2View.as_view()),
    url(r'page3/', Page3View.as_view()),



    url(r'^polls/', include('polls.urls')),
    url(r'^admin/', admin.site.urls),
]